import time
import sys
import random

def delay_print(s):
    for c in s:
        sys.stdout.write(c)
        sys.stdout.flush()
        time.sleep(0.001)
def card_print():
    txt1 ="                                          88            \n"
    txt2 ="                                          88            \n"
    txt3 ="                                          88            \n"
    txt4 =" ,adPPYba, ,adPPYYba, 8b,dPPYba,  ,adPPYb,88 ,adPPYba,  \n"
    txt5 ='a8"     "" ""     `Y8 88P\'   "Y8 a8"    `Y88 I8[  "\"  \n'
    txt6 ="8b         ,adPPPPP88 88         8b       88  `\"Y8ba,   \n"
    txt7 ="\"8a,   ,aa 88,    ,88 88         \"8a,   ,d88 aa    ]8I  \n"
    txt8 =" `\"Ybbd8\"\' `\"8bbdP\"Y8 88          `\"8bbdP\"Y8 `\"YbbdP\"\' \n"
    txt9 ="                                                 Blackjack Edition"
    delay_print(txt1)
    delay_print(txt2)
    delay_print(txt3)
    delay_print(txt4)
    delay_print(txt5)
    delay_print(txt6)
    delay_print(txt7)
    delay_print(txt8)
    time.sleep(2)
    delay_print(txt9)

#card_print()

cards = {
"Ace - Heart": 1,"Ace - Clubs": 1,"Ace - Diamonds": 1,"Ace - Spades": 1,
"Two - Heart": 2,"Two - Clubs": 2,"Two - Diamonds": 2,"Two - Spades": 2,
"Three - Heart": 3,"Three - Clubs": 3,"Three - Diamonds": 3,"Three - Spades": 3,
"Four - Heart": 4,"Four - Clubs": 4,"Four - Diamonds": 4,"Four - Spades": 4,
"Five - Heart": 5,"Five - Clubs": 5,"Five - Diamonds": 5,"Five - Spades": 5,
"Six - Heart": 6,"Six - Clubs": 6,"Six - Diamonds": 6,"Six - Spades": 6,
"Seven - Heart": 7,"Seven - Clubs": 7,"Seven - Diamonds": 7,"Seven - Spades": 7,
"Eight - Heart": 8,"Eight - Clubs": 8,"Eight - Diamonds": 8,"Eight - Spades": 8,
"Nine - Heart": 9,"Nine - Clubs": 9,"Nine - Diamonds": 9,"Nine - Spades": 9,
"Ten - Heart": 10,"Ten - Clubs": 10,"Ten - Diamonds": 10,"Ten - Spades": 10,
"Jack - Heart": 10,"Jack - Clubs": 10,"Jack - Diamonds": 10,"Jack - Spades": 10,
"Queen - Heart": 10,"Queen - Clubs": 10,"Queen - Diamonds": 10,"Queen - Spades": 10,
"King - Heart": 10,"King - Clubs": 10,"King - Diamonds": 10,"King - Spades": 10,
}

current_cards = cards.copy()

original_cards = cards.copy()
current_cards = original_cards.copy()

my_current_total = 0
my_cards = []
hacksaw_total = 0
hacksaw_cards = []

hacksaw_wins = 0
my_wins = 0
num_aces = 0

def calculate_ace(total, num_aces):
    """Calculate the value of ace(s) as 1 or 11"""
    if num_aces == 0:
        return total, num_aces

    # If adding 11 doesn't cause bust, use it.
    if total + 11 <= 21:
        total += 10
        num_aces -= 1
    else:
        total += 0
        num_aces -= 1

    # If two aces are in hand, use one as 1 to avoid bust.
    if num_aces > 0 and total > 21:
        total -= 10
        num_aces -= 1

    return total, num_aces

def draw(current_total, current_cards, num_aces):
    draw = random.choice(list(current_cards))
    current_total += current_cards[draw]
    ace = 0
    if "Ace" in draw:
        ace += 1
        current_total, num_aces = calculate_ace(current_total, ace)
        del current_cards[draw]
        return current_total, draw, num_aces
    else:
        del current_cards[draw]
        return current_total, draw, num_aces

def reset():
    global my_current_total
    global hacksaw_total
    global current_cards
    global hacksaw_cards
    global my_cards
    global num_aces
    my_current_total = 0
    hacksaw_total = 0
    current_cards = original_cards.copy()
    hacksaw_cards = []
    my_cards = []
    num_aces = 0

print("Draw a card:")


print("You're seated at a table in Silk Johnny's fine (seedy) establishment. A haze of smoke fills the air, of which, a single shaft of light from a bare bulb cuts through.")
print("You can hear the other patrons carousing behind a large door somewhere behind you, but your attention is focused on the green felt table in front of you.")
print("Hacksaw Gresham sits across from you, a large cigar hanging between his lips.")
print("\"Alright,\" Hacksaw says, \"Quit screwin' around and lets get to playing.\"")
print("")
print("You know if you can just beat Hacksaw 10 times before he does, you'll have secured the game. If he wins... well, you don't want to think about that.")

while my_wins < 10 or hacksaw_wins < 10:
    print("I have " + str(my_wins) + " wins and Hacksaw has: " + str(hacksaw_wins) + ".")
    print("You are dealt a card.")
    my_current_total, my_card, num_aces = draw(my_current_total, current_cards, num_aces)
    my_cards.append(my_card)
    print("You are dealt \033[1;32m{}\033[0m and your total is \033[1;32m{}\033[0m".format(my_card, my_current_total))
    print("")
    input("Next...")

    print("Hacksaw is dealt a card.")
    hacksaw_total, hacksaw_card, num_aces = draw(hacksaw_total, current_cards, num_aces)
    print("Hacksaw was dealt \033[1;32m{}\033[0m".format(hacksaw_card))
    hacksaw_cards.append(hacksaw_card)
    print("")
    input("Next...")

    print("You are dealt another card.")
    my_current_total, my_card, num_aces = draw(my_current_total, current_cards, num_aces)
    my_cards.append(my_card)
    print("You are dealt \033[1;32m{}\033[0m and your total is now \033[1;32m{}\033[0m".format(my_card, my_current_total))
    print("")
    input("Next...")

    print("Hacksaw is dealt a card.")
    #hacksaw_total, my_card, num_aces = draw(my_current_total, current_cards, num_aces)
    hacksaw_total, hacksaw_card, num_aces = draw(hacksaw_total, current_cards, num_aces)
    hacksaw_cards.append(hacksaw_card)
    print("")


    game_over = False

    while my_current_total <= 21 and not game_over:
        user_input = input("Your total is: \033[1;32m" + str(my_current_total) + "\033[0m Would you like to hit or stand? ")
        if user_input == "hit":
            my_current_total, my_card, num_aces = draw(my_current_total, current_cards, num_aces)
            my_cards.append(my_card)
            print("You are dealt \033[1;32m{}\033[0m and your total is now \033[1;32m{}\033[0m".format(my_card, my_current_total))
        else:
            print("Cool, you stand. Your total is: \033[1;32m"+ str(my_current_total) + "\033[0m")
            if hacksaw_total <= 21 and hacksaw_total >= 16:
                print("Hacksaw stands.")
                input("Next...")
                print("You both reveal your hands.")
                print("You have: ")
                print(' '.join(my_cards))
                print("Your cards total: " + str(my_current_total))
                print("")
                print("Hacksaw has: ")
                print(' '.join(hacksaw_cards))
                print("Hacksaw's cards total: " +str(hacksaw_total))
                if my_current_total == hacksaw_total:
                        print("Push!")
                        print("")
                        print("The cards are shuffled and you go again.")
                        reset()
                        game_over = True
                        break
                elif my_current_total > hacksaw_total:
                    print("You \u001b[33mwin\033[0m!")
                    my_wins +=1
                    print("")
                    print("The cards are shuffled and you go again.")
                    reset()
                    game_over = True
                else:
                    print("Hacksaw \u001b[33mwins\033[0m!")
                    hacksaw_wins += 1
                    reset()
                    game_over = True
            while hacksaw_total < 16 and not game_over:
                hacksaw_total, hacksaw_card, num_aces = draw(hacksaw_total, current_cards, num_aces)
                hacksaw_cards.append(hacksaw_card)
                print("Hacksaw draws a card.")
                if hacksaw_total > 21:
                    print("Hacksaw \u001b[31mbusts!\033[0m! You \u001b[33mwin\033[0m!")
                    print("The cards are shuffled and you go again.")
                    print("")
                    reset()
                    my_wins +=1
                    game_over = True
                    break
                elif hacksaw_total <= 21 and hacksaw_total >= 16:
                    print("Hacksaw stands.")
                    input("Next...")
                    print("You both reveal your hands.")
                    print("You have: ")
                    print(' '.join(my_cards))
                    print("Your cards total: " + str(my_current_total))
                    print("")
                    print("Hacksaw has: ")
                    print(' '.join(hacksaw_cards))
                    print("Hacksaw's cards total: " +str(hacksaw_total))
                    if my_current_total == hacksaw_total:
                        print("Push!")
                        print("")
                        print("The cards are shuffled and you go again.")
                        reset()
                        game_over = True
                        break
                    elif my_current_total > hacksaw_total:
                        print("You \u001b[33mwin\033[0m!")
                        my_wins +=1
                        print("")
                        print("The cards are shuffled and you go again.")
                        reset()
                        game_over = True
                        break

                    else:
                        print("Hacksaw \u001b[33mwins\033[0m!")
                        hacksaw_wins += 1
                        reset()
                        game_over = True
                        break
        if my_current_total > 21:
            print("\u001b[31mBust!\033[0m! Hacksaw \u001b[33mwin\033[0ms!")
            hacksaw_wins += 1
            reset()
            print("The cards are shuffled and you go again.")
            print("")
            game_over = True

        elif hacksaw_total > 21:
            print("Hacksaw \u001b[31mbusts!\033[0m!")
            my_wins += 1
            reset()
            print("The cards are shuffled and you go again.")
            print("")
            game_over = True

    if my_wins == 10:

        break

    if hacksaw_wins == 10:

        break

if my_wins > 9:
    print("Hacksaw grunts and throws his cards down.")
    print("\"Fine, you win,\" he grunts. \"C'mon, the boss wants to see you.\"")
elif hacksaw_wins > 9:
    print("Hacksaw jumps up. \"HA! I WIN! C'mon, the boss wants to see you.\"")
    time.sleep(1)
print("Hacksaw leads you into Silk Johnny's office. If someone's diet only consisted of purple velvet, and they had a case of explosive vomit, it would look similar to how Johnny's office was decorated.")
print("Hacksaw closes the door and stands in front of it.")
input("Next..")
print("Silk Johnny stands up and grimaces at you.")
print("\"You already owe me fifty G's and now you come in here and start trying to take money from my boys?\"")
print("\"I'm gettin' real sick of you,\" he grunts. \"But I'm a reasonable guy... I got a way to have this all end amicably.\"")
time.sleep(.5)
print("\"I want you dead, and you like to gamble... So let's gamble.\"")
input("Next...")
print("Silk Johnny suddenly whips out a snub-nosed revolver. But, instead of pointing it at you he spins the cylinder, places the barrel against his temple and pulls the trigger.")
print("\u001b[31mClick!\033[0m")
input("Next...")
print("He opens the cylinder and shows you that there's only one bullet loaded.")
print("\"Your turn,\" he says, before shoving the revolver into your hands, \"and don't try no funny business, 'cuz Hacksaw there doesn't carry a revolver.\"")
print("You hear Hacksaw chamber a bullet into his glock.")

cylinder = ""
gun_loaded = False


def dead(who):
    i = 0
    i2 = 0
    num = 1
    while i2 < 100:
        while i < num:
            print("\x1b[48;2;255;0;0m", end='')  # set background color to red
            print("Dead.")
            time.sleep(.001)
            num = random.randint(1,3)
            i += 1
        while i > 0:
            print("\x1b[0m", end='')  # reset to default background color
            print("Dead.")
            time.sleep(.001)
            i -= 1
        i2 += 1

    print("\x1b[0m", end='')  # reset to default background color

    if who == "You":
        print("Holy fuck, you shot yourself in the head. Wow, way to go! Game over!")
        death1 = "Congrats, you shot yourself in the head!"

        for i in range(len(death1)):
            print(death1[i], sep= ' ',end=' ', flush = True); time.sleep(0.5)
        time.sleep(2)
        exit()
    if who == "Johnny":
        print("Holy shit, Johnny just blew his brains out all over the room.")
        print("It actually makes the decor a little bit better.")
        time.sleep(1)
        print("Hacksaw's eyes are huge and he watches you walk out of the room.")
        print("At this point, your debt has been cleared.")
        time.sleep(1)
        death1 = "Congrats, you didn't shoot yourself in the head!"

        for i in range(len(death1)):
            print(death1[i], sep= ' ',end=' ', flush = True); time.sleep(0.5)
        time.sleep(2)
        exit()



def fire():
    global gun_loaded
    print("You place the gun against your temple...")
    while gun_loaded == True:

        gun_input = input("Type squeeze to pull the trigger: ")
        if gun_input == "squeeze":
            print("The revolver's cylinder spins and then clicks into place.")
            cylinder = random.randint(0,6)

            if cylinder < 1:

                print("BANG")
                print("You dead")
                dead("You")
            else:
                print("\u001b[31mClick!\033[0m")
                time.sleep(1)
                print("\"Heh,\" Silk Johnny laughs, \"Alright, my turn.\"")
                print("He takes the gun from you, spins the cylinder, puts the barrel to his temple and pulls the trigger.")
                input("Next...")
                cylinder = random.randint(0,6)
                if cylinder < 1:
                    print("BANG")
                    print("Dead")
                    dead("Johnny")
                else:
                    gun_loaded = False
                    print("\u001b[31mClick!\033[0m")
                    print("\"Your turn,\" he says.")
                    break
        elif gun_input == "quit":
            print("Exiting...")
            break
        else:
            print("\"C'mon!\"Just fuckin' do it.\"")

while True:
    gun_input = input("Type spin to spin the cylinder: ")
    cylinder = None
    if gun_input == "spin":
        print("The revolver's cylinder spins and then clicks into place.")
        cylinder = random.randint(0,6)
        gun_loaded = True
        fire()
    elif gun_input == "quit":
        print("Exiting...")
        break
    else:
        print("\"C'mon!\" Silk Johnny yells, \"I ain't got all day!\"")
